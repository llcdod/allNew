package com.englishvoc.allproject;

        import android.content.Context;
        import android.support.annotation.NonNull;
        import android.support.v4.view.PagerAdapter;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.RelativeLayout;
        import android.widget.TextView;

        import java.util.Objects;

public class SlideAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;

    public SlideAdapter(Context context){
        this.context = context;
    }
    //Arry
    public int[] slide_images = {
            R.drawable.children,
            R.drawable.housew,
            R.drawable.shopkpr
    };

    public String[] slide_heading = {
            "আমি ফাহিম",
            "আমি নাসিমা",
            "আমি করিম"
    };

    public String[] slide_description = {
            "আমি একজন ছাত্র। আমার English app এ প্রতিদিন আমি ইংরেজি শিখি।",
            "আমি একজন গ্রিহিনি। আমার English app এ প্রতিদিন আমি ইংরেজি শিখি।",
            "আমি একজন মুদি দোকানদার। আমার English app এ প্রতিদিন আমি ইংরেজি শিখি।"
    };

    @Override
    public int getCount() {
        return slide_description.length;
    }

    @Override
    public boolean isViewFromObject( View view, Object o) {
        return view == (RelativeLayout) o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.imageView);
        TextView slideHeadingView = (TextView) view.findViewById(R.id.slide_heading);
        TextView slideDesView = (TextView) view.findViewById(R.id.slide_des);

        slideImageView.setImageResource(slide_images[position]);
        slideHeadingView.setText(slide_heading[position]);
        slideDesView.setText(slide_description[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }
}