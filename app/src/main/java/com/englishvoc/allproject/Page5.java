package com.englishvoc.allproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Page5 extends AppCompatActivity implements View.OnClickListener{
    UserDetails userDetails;
    DatabaseHelper databaseHelper;
    AlertDialog.Builder builder;
    private RadioButton lssn1q1, lssn1q2, lssn1q3, lssn1q4;
    LinearLayout lssn1q1laout,lssn1q2laout,lssn1q3laout,lssn1q4laout;
    //database
    String ID = "1";
    String lession;
    String lession1 = "1";
    String lession2 = "2";
    String lession3 = "3";
    String lession4 = "4";
    String lession5 = "5";
    String lession6 = "6";
    String lession7 = "7";
    String lession8 = "8";
    String lession9 = "9";
    String lession10 = "10";
    String lession11 = "11";
    String lession12 = "12";
    TextView l1page5H1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page5);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        databaseHelper = new DatabaseHelper(this);
        //  subHeadingId = (TextView)findViewById(R.id.subHeadingId);
        //radioCheck
        lssn1q1 = (RadioButton) findViewById(R.id.lssn1q1);
        lssn1q2 = (RadioButton) findViewById(R.id.lssn1q2);

        lssn1q1.setOnClickListener(this);
        lssn1q2.setOnClickListener(this);

        lssn1q1laout =(LinearLayout) findViewById(R.id.lssn1q1laout);
        lssn1q2laout =(LinearLayout) findViewById(R.id.lssn1q2laout);

        //button
        AppCompatButton continuousBotton = (AppCompatButton) findViewById(R.id.continuousBotton);
        continuousBotton.setOnClickListener(this);
        AppCompatButton backBotton = (AppCompatButton) findViewById(R.id.backBotton);
        backBotton.setOnClickListener(this);
        //helpId
        AppCompatButton helpId = (AppCompatButton) findViewById(R.id.helpId);
        helpId.setOnClickListener(this);
        //main containt
        l1page5H1 = (TextView)findViewById(R.id.l1page5H1);

        //databse query
        Boolean result = databaseHelper.findUser(ID);
        if(result==true) {
            Cursor cursor = databaseHelper.displayData();
            if (cursor.getCount() == 0) { Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
                return; }else {
                if(cursor.moveToFirst()){
                    do{
                        lession = cursor.getString(cursor.getColumnIndex("Lession"));
                        lessionFind();
                    }while (cursor.moveToNext());
                }
                cursor.close();
            }
            Toast.makeText(getApplicationContext(), "Lession "+lession, Toast.LENGTH_LONG).show();
        }else{
            Intent i = new Intent(Page5.this, Dhashboard.class);
            startActivity(i);
            Toast.makeText(getApplicationContext(), "Lession didn't match", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.lssn1q1:
                /* Button 1*/
                if(lession.equals(lession1)){
                    questionButton1True();
                }else if(lession.equals(lession2)){
                    questionButton1false();
                }
                break;
            case R.id.lssn1q2:
                /* Button 2*/
                if(lession.equals(lession1)){
                    questionButton2False();
                }else if(lession.equals(lession2)){
                    questionButton2True();
                }
                break;
            case R.id.continuousBotton:
                Intent i = new Intent(Page5.this, Page6.class);
                //get data
                Intent intent = getIntent();
                if (intent.hasExtra("coin")) {
                    i.putExtra("coin", "10");
                } else {
                    i.putExtra("coin", "10");
                }
                startActivity(i);
                break;
            case R.id.backBotton:
                Intent i2 = new Intent(Page5.this, Page4.class);
                startActivity(i2);
                break;
            case R.id.helpId:
                helpPage();
                break;
        }
    }
    /* Button 1 true ------------------------------------------------------------------------*/
    public void questionButton1True(){
        lssn1q1.setChecked(true);
        lssn1q2.setChecked(false);
        lssn1q1laout.setBackgroundColor(getResources().getColor(R.color.green));
        lssn1q2laout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        answareTrue();
    }
    /* Button 1 false */
    public void questionButton1false(){
        lssn1q1.setChecked(false);
        lssn1q2.setChecked(false);
        lssn1q1laout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        lssn1q2laout.setBackgroundColor(getResources().getColor(R.color.white));
        answareFalse();
    }
    /* Button 2 True-----------------------------------------------------------------------*/
    public void questionButton2True(){
        lssn1q1.setChecked(false);
        lssn1q2.setChecked(true);
        lssn1q1laout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        lssn1q2laout.setBackgroundColor(getResources().getColor(R.color.green));
        answareTrue();
    }
    /* Button 2 false */
    public void questionButton2False(){
        lssn1q1.setChecked(false);
        lssn1q2.setChecked(false);
        lssn1q1laout.setBackgroundColor(getResources().getColor(R.color.white));
        lssn1q2laout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        answareFalse();
    }

    /*--------------------------------------------------------------------------------------*/
    //find lession
    public void lessionFind(){
        if(lession.equals(lession1)){
            l1page5H1.setText(getString(R.string.l1page5H1));
            lssn1q1.setText(getString(R.string.l1page5Q1));
            lssn1q2.setText(getString(R.string.l1page5Q2));
        }else if(lession.equals(lession2)){
            l1page5H1.setText(getString(R.string.l2page5H1));
            lssn1q1.setText(getString(R.string.l2page5Q1));
            lssn1q2.setText(getString(R.string.l2page5Q2));
        }else if(lession.equals(lession3)){

        }
    }

    public void answareFalse(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(Page5.this);
        builder.setTitle("ভুল !");
        builder.setMessage("আপনার উত্তর সঠিক নয়!");
        builder.setNegativeButton("পরবর্তী", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {
                Intent i = new Intent(Page5.this, Page6.class);
                startActivity(i);
            }
        });
        builder.setPositiveButton("আবার চেষ্টা করুন",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog,int which)
                    {
                        finish();
                        startActivity(getIntent());
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    public void answareTrue(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(Page5.this);
        builder.setTitle("সঠিক !");
        builder.setMessage("আপনি সঠিক উত্তর দিয়েছেন !");
        builder.setNegativeButton("পরবর্তী", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {
                Intent i = new Intent(Page5.this, Page6.class);
                //get data
                Intent intent = getIntent();
                if (intent.hasExtra("coin")) {
                    i.putExtra("coin", "50");
                } else {
                    i.putExtra("coin", "10");
                }
                startActivity(i);
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void helpPage(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(Page5.this);
        builder.setTitle("Help !");
        builder.setMessage("সঠিক উত্তরটি খুজে বের করে তাতে ঠিক দিন!!");
        builder.setNegativeButton("ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
