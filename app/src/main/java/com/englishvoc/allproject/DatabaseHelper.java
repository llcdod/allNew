package com.englishvoc.allproject;

        import android.content.ContentValues;
        import android.content.Context;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "userdetails.db";
    private static final String TABLE_NAME = "user_details";
    private static final String ID = "Id";
    private static final String NAME = "Name";
    private static final String PROFESSION = "Profession";
    private static final String LESSION = "Lession";
    private static final String COIN = "Coin";
    private static final int VERSION_NUMBER = 1;
    private Context context;


    private static final String CREATE_TABLE = " CREATE TABLE "+TABLE_NAME+"("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+NAME+" VARCHAR(255) NOT NULL,"+PROFESSION+" TEXT NOT NULL,"+LESSION+" TEXT NOT NULL,"+COIN+" TEXT NOT NULL)";

    private static final String DROP_TABLE = " DROP TABLE IF EXISTS "+TABLE_NAME;
    //private static final String SELECT_ALL = " SELECT * FROM "+TABLE_NAME;
    private static final String SELECT_ALL = " SELECT * FROM "+TABLE_NAME+" WHERE "+ID+" = 1";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_NUMBER);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try{
            db.execSQL(CREATE_TABLE);
            Toast.makeText(context,"onCreate is called",Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(context,"Exception : "+e,Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try{
            Toast.makeText(context,"onUpgrade is called",Toast.LENGTH_LONG).show();
            db.execSQL(DROP_TABLE);
            onCreate(db);

        }catch (Exception e){
            Toast.makeText(context,"Exception : "+e,Toast.LENGTH_LONG).show();
        }

    }

    public long insertData(UserDetails userDetails){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,userDetails.getName());
        contentValues.put(PROFESSION,userDetails.getProfession());
        contentValues.put(LESSION,userDetails.getLession());
        contentValues.put(COIN,userDetails.getCoin());

        long rowId = db.insert(TABLE_NAME,null,contentValues);
        return rowId;


    }

    public Boolean findUser(String uname){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        Boolean result = false;

        if(cursor.getCount()==0)
        {
            Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
        }else{
            while(cursor.moveToNext()){
                String idusername = cursor.getString(0);
                if(idusername.equals(uname)){
                    result = true;
                    break;
                }
            }
        }
        return result;

    }

    public boolean updateData(String id, String lession,String coin){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID,id);
        contentValues.put(LESSION,lession);
        contentValues.put(COIN,coin);

        db.update(TABLE_NAME,contentValues,ID+" = ? ",new String[]{ id } );
        return true;
    }

    public Cursor displayData(){
        SQLiteDatabase db = this.getWritableDatabase();
       Cursor cursor = db.rawQuery(SELECT_ALL,null);
       return cursor;
    }
}
