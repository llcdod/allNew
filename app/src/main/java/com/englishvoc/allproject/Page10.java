package com.englishvoc.allproject;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class Page10 extends AppCompatActivity {
    //database
    String ID = "1";
    String lession;
    String lession1 = "1";
    String lession2 = "2";
    String lession3 = "3";
    String lession4 = "4";
    String lession5 = "5";
    String lession6 = "6";
    String lession7 = "7";
    String lession8 = "8";
    String lession9 = "9";
    String lession10 = "10";
    String lession11 = "11";
    String lession12 = "12";
    //TextView subHeadingId;
    UserDetails userDetails;
    DatabaseHelper databaseHelper;
    PieChartView pieChartView;
    Button yesB;
    Button noButton;
    String rightR;
    String wrongR;
    int lessonMain;
    String leCoin;
    int lessonCoin;

    int rightR1;
    int lessonCoinMain;
    String coin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page10);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userDetails = new UserDetails();
        databaseHelper = new DatabaseHelper(this);

        //databse query
        Boolean result = databaseHelper.findUser(ID);
        if(result==true) {
            Cursor cursor = databaseHelper.displayData();
            if (cursor.getCount() == 0) { Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
                return; }else {
                if(cursor.moveToFirst()){
                    do{
                        lession = cursor.getString(cursor.getColumnIndex("Lession"));
                        leCoin = cursor.getString(cursor.getColumnIndex("Coin"));
                        lessonMain = Integer.parseInt(lession.replaceAll("[\\D]", "")); //convert string to int
                        lessonCoin = Integer.parseInt(leCoin.replaceAll("[\\D]", "")); //convert string to int
                    }while (cursor.moveToNext());
                }
                cursor.close();
            }
            Toast.makeText(getApplicationContext(), "Lesson "+lession, Toast.LENGTH_LONG).show();
        }else{
            Intent i = new Intent(Page10.this, Dhashboard.class);
            startActivity(i);
            Toast.makeText(getApplicationContext(), "Lession didn't match", Toast.LENGTH_LONG).show();
        }




        //get data
        Intent intent = getIntent();
        if (intent.hasExtra("finaleCorrResult")) {
            Bundle extras = getIntent().getExtras();
            rightR= extras.getString("finaleCorrResult");
         //   wrongR = getIntent().getExtras().getString("finaleWrongResult","0");
        } else {
            Bundle extras = getIntent().getExtras();
            rightR= extras.getString("finaleCorrResult");
        //    wrongR = getIntent().getExtras().getString("finaleWrongResult","0");
        }


        //button
        Button continuousBotton = (Button) findViewById(R.id.noButton);
        continuousBotton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                String id = "1";
                //lesson
               int lessonFinale = lessonMain+1; // increament lesson
               String lession = Integer.toString(lessonFinale); //convert int to string
                //coin
                rightR1= Integer.parseInt(rightR.replaceAll("[\\D]", ""));
                lessonCoinMain = lessonCoin+rightR1;
                coin = Integer.toString(lessonCoinMain); //convert int to string

                Boolean isUpdated =  databaseHelper.updateData(id,lession,coin);
                if(isUpdated==true){
                    Toast.makeText(getApplicationContext(),"Data update is successfully",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Page10.this, MainActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(),"Data update is not successfully",Toast.LENGTH_LONG).show();
                }
            }
        });
        Button startagain = (Button) findViewById(R.id.yesB);
        startagain.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                Intent i = new Intent(Page10.this, MainActivity.class);
                startActivity(i);
            }
        });

        pieChartView = findViewById(R.id.chart);
        List pieData = new ArrayList<>();
      //  int wrongR1=Integer.parseInt(wrongR.replaceAll("[\\D]", ""));
        int rightR1= Integer.parseInt(rightR.replaceAll("[\\D]", ""));
        int wrongR1= rightR1 - 100;
        pieData.add(new SliceValue(rightR1, Color.GREEN).setLabel("সঠিক: "+rightR+"%"));
        pieData.add(new SliceValue(wrongR1, Color.RED).setLabel("ভুল: "+wrongR1+"%"));

        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData.setHasCenterCircle(true).setCenterText1("ফলাফল").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
        pieChartView.setPieChartData(pieChartData);

        //button for result
        TextView allScore1 = (TextView) findViewById(R.id.allScore);
        allScore1.setText("আপনার এই পর্যন্ত স্কোর : "+leCoin);
        TextView nowScore1 = (TextView) findViewById(R.id.nowScore);
        nowScore1.setText("এখন কার স্কোর : "+rightR1);
        TextView warming = (TextView)findViewById(R.id.warming);
        if(rightR1 <= 10){
            warming.setText("আপনি খুব ভালো পারেননি ।");
            warming.setTextColor(getResources().getColor(R.color.colorAccent));
        }else {
            warming.setText("আপনি খুব ভালো চেষ্টা করেছেন।");
            warming.setTextColor(getResources().getColor(R.color.green));
        }
    }
}