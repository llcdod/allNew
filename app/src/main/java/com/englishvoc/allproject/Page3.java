package com.englishvoc.allproject;

import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.LinearGradient;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.DragEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class Page3 extends AppCompatActivity implements View.OnClickListener {

    LinearLayout target1,target2,target3,target4;
    Button btn1,btn2,btn3,btn4,button1,button2,button3,button4;

    //database
    String ID = "1";
    String lession;
    String lession1 = "1";
    String lession2 = "2";
    String lession3 = "3";
    String lession4 = "4";
    String lession5 = "5";
    String lession6 = "6";
    String lession7 = "7";
    String lession8 = "8";
    String lession9 = "9";
    String lession10 = "10";
    String lession11 = "11";
    String lession12 = "12";
    UserDetails userDetails;
    DatabaseHelper databaseHelper;
    TextView  subHeadingId;

    AppCompatButton continuousBotton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page3);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        target1=(LinearLayout)findViewById(R.id.target1);
        target2=(LinearLayout)findViewById(R.id.target2);
        target3=(LinearLayout)findViewById(R.id.target3);
        target4=(LinearLayout)findViewById(R.id.target4);


        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);

        btn1.setOnLongClickListener(longClickListener);
        btn2.setOnLongClickListener(longClickListener);
        btn3.setOnLongClickListener(longClickListener);
        btn4.setOnLongClickListener(longClickListener);

        target1.setOnDragListener(dragListener);
        target2.setOnDragListener(dragListener);
        target3.setOnDragListener(dragListener);
        target4.setOnDragListener(dragListener);

        //text
        //main containt
        subHeadingId = (TextView)findViewById(R.id.l1page3H1);

        //button to next and back
        continuousBotton = (AppCompatButton) findViewById(R.id.continuousBotton);
        continuousBotton.setOnClickListener(this);
        continuousBotton.setVisibility(View.GONE);
        AppCompatButton backBotton = (AppCompatButton) findViewById(R.id.backBotton);
        backBotton.setOnClickListener(this);
        //helpId
        AppCompatButton helpId = (AppCompatButton) findViewById(R.id.helpId);
        helpId.setOnClickListener(this);
        //DATABASE
        userDetails = new UserDetails();
        databaseHelper = new DatabaseHelper(this);
        //databse query
        Boolean result = databaseHelper.findUser(ID);
        if(result==true) {
            Cursor cursor = databaseHelper.displayData();
            if (cursor.getCount() == 0) { Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
                return; }else {
                if(cursor.moveToFirst()){
                    do{
                        lession = cursor.getString(cursor.getColumnIndex("Lession"));
                        lessionFind();
                    }while (cursor.moveToNext());
                }
                cursor.close();
            }
            Toast.makeText(getApplicationContext(), "Lesson "+lession, Toast.LENGTH_LONG).show();
        }else{
            Intent i = new Intent(Page3.this, Dhashboard.class);
            startActivity(i);
            Toast.makeText(getApplicationContext(), "Lesson didn't match", Toast.LENGTH_LONG).show();
        }

    }

    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            ClipData data = ClipData.newPlainText("","");
            View.DragShadowBuilder myShadowBuilder = new View.DragShadowBuilder(v);
            v.startDrag(data,myShadowBuilder,v,0);
            return false;
        }
    };
    View.OnDragListener dragListener = new View.OnDragListener() {
        @Override
        public boolean onDrag(View v, DragEvent event) {

            int dragEvent = event.getAction();
            final View view = (View) event.getLocalState();
            switch(dragEvent){

                case DragEvent.ACTION_DRAG_ENTERED:

                break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                   if(view.getId() == R.id.btn1 && v.getId() == R.id.target1){

                       LinearLayout oldParent = (LinearLayout)view.getParent();
                       oldParent.removeView(view);
                       LinearLayout newParent = (LinearLayout)v;
                       button1.setVisibility(View.GONE);
                       newParent.addView(view);
                   }else if(view.getId() == R.id.btn2 && v.getId() == R.id.target2){

                       LinearLayout oldParent = (LinearLayout)view.getParent();
                       oldParent.removeView(view);
                       LinearLayout newParent = (LinearLayout)v;
                       button2.setVisibility(View.GONE);
                       newParent.addView(view);
                   }else if(view.getId() == R.id.btn3 && v.getId() == R.id.target3){

                       LinearLayout oldParent = (LinearLayout)view.getParent();
                       oldParent.removeView(view);
                       LinearLayout newParent = (LinearLayout)v;
                       button3.setVisibility(View.GONE);
                       newParent.addView(view);
                   }else if(view.getId() == R.id.btn4 && v.getId() == R.id.target4){

                       LinearLayout oldParent = (LinearLayout)view.getParent();
                       oldParent.removeView(view);
                       LinearLayout newParent = (LinearLayout)v;
                       button4.setVisibility(View.GONE);
                       newParent.addView(view);
                       continuousBotton.setVisibility(View.VISIBLE);
                   }
                    break;
            }

            return true;
        }
    };

    //button to next and back
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.continuousBotton:
                Intent i = new Intent(Page3.this, Page4.class);
                //get data
                Intent intent = getIntent();
                if (intent.hasExtra("coin")) {
                    i.putExtra("coin", "30");
                } else {
                    i.putExtra("coin", "10");
                }
                startActivity(i);
                break;
            case R.id.backBotton:
                Intent i2 = new Intent(Page3.this, Page2.class);
                startActivity(i2);
                break;
            case R.id.helpId:
                helpPage();
                break;
        }
    }
    //find lesson
    public void lessionFind(){
        if(lession.equals(lession1)){
            subHeadingId.setText(getString(R.string.l1page3H1));
            //  subtextId.setText(getString(R.string.subtextId));
            btn1.setText(getString(R.string.l1page3B1));
            btn2.setText(getString(R.string.l1page3B2));
            btn3.setText(getString(R.string.l1page3B3));
            btn4.setText(getString(R.string.l1page3B4));
        }else if(lession.equals(lession2)){
            subHeadingId.setText(getString(R.string.l2page3H1));
            //  subtextId.setText(getString(R.string.subtextId));
            btn1.setText(getString(R.string.l2page3B1));
            btn2.setText(getString(R.string.l2page3B2));
            btn3.setText(getString(R.string.l2page3B3));
            btn4.setText(getString(R.string.l2page3B4));
        }else if(lession.equals(lession3)){
            subHeadingId.setText("শিখুন 3");
        }
    }
    public void helpPage(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(Page3.this);
        builder.setTitle("Help !");
        builder.setMessage("এলোমেলো বাটোন চেপে ধরে ড্রাগ এন্ড ড্রপ করে বাক্যটি সঠিক করুন!");
        builder.setNegativeButton("ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
