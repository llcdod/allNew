package com.englishvoc.allproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Page1 extends AppCompatActivity {
    String lession1 = "1";
    String lession2 = "2";
    String lession3 = "3";
    String lession4 = "4";
    String lession5 = "5";
    String lession6 = "6";
    String lession7 = "7";
    String lession8 = "8";
    String lession9 = "9";
    String lession10 = "10";
    String lession11 = "11";
    String lession12 = "12";

    TextView subHeadingId,subtextId,firstId,secondId,thirdId,fourthId;
    //find id
    DatabaseHelper databaseHelper;
    UserDetails userDetails;
    String ID = "1";
    String lession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page1);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //main containt
        subHeadingId = (TextView)findViewById(R.id.subHeadingId);
       // subtextId = (TextView)findViewById(R.id.subtextId);
        firstId = (TextView)findViewById(R.id.firstId);
        secondId = (TextView)findViewById(R.id.secondId);
        thirdId = (TextView)findViewById(R.id.thirdId);
        fourthId = (TextView)findViewById(R.id.fourthId);

        //database
        userDetails = new UserDetails();
        databaseHelper = new DatabaseHelper(this);
        //databse query
        Boolean result = databaseHelper.findUser(ID);
        if(result==true) {

            Cursor cursor = databaseHelper.displayData();
            if (cursor.getCount() == 0) {
                //no data
                Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
                return;
            } else {
                if(cursor.moveToFirst()){
                    do{
                        lession = cursor.getString(cursor.getColumnIndex("Lession"));
                        lessionFind();
                    }while (cursor.moveToNext());
                }
                cursor.close();
            }
            Toast.makeText(getApplicationContext(), "Lesson "+lession, Toast.LENGTH_LONG).show();
        }else{
            Intent i = new Intent(Page1.this, Dhashboard.class);
            startActivity(i);
            Toast.makeText(getApplicationContext(), "Lesson didn't match", Toast.LENGTH_LONG).show();
        }
        //buttonNext
        AppCompatButton continuousBotton = (AppCompatButton) findViewById(R.id.continuousBotton);
        continuousBotton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                Intent i = new Intent(Page1.this, Page2.class);
                startActivity(i);
            }
        });
        //buttonBack
        AppCompatButton backBotton = (AppCompatButton) findViewById(R.id.backBotton);
        backBotton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                Intent i = new Intent(Page1.this, MainActivity.class);
                startActivity(i);
            }
        });
        //helpId
        AppCompatButton helpId = (AppCompatButton) findViewById(R.id.helpId);
        helpId.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                helpPage1();
            }
        });
    }
  public void lessionFind(){
        if(lession.equals(lession1)){
            subHeadingId.setText(getString(R.string.subHeadingId));
          //  subtextId.setText(getString(R.string.subtextId));
            firstId.setText(getString(R.string.firstId));
            secondId.setText(getString(R.string.secondId));
            thirdId.setText(getString(R.string.thirdId));
            fourthId.setText(getString(R.string.fourthId));
        }else if(lession.equals(lession2)){
            subHeadingId.setText(getString(R.string.subHeadingId2));
            subHeadingId.setVisibility(View.GONE);
         //   subtextId.setText(getString(R.string.subtextId2));
            firstId.setText("হ্যালো, আপনি কেমন আছেন ? আমি ভাল আছি ইত্যাদি বলা শিখুন ");
            secondId.setVisibility(View.GONE);
            thirdId.setVisibility(View.GONE);
            fourthId.setVisibility(View.GONE);
          //  secondId.setText(getString(R.string.secondId2));
          //  thirdId.setText(getString(R.string.thirdId2));
            fourthId.setText(getString(R.string.fourthId2));
        }else if(lession.equals(lession3)){
            subHeadingId.setText("শিখুন 3");
        }
    }

    public void helpPage1(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(Page1.this);
        builder.setTitle("সাহায্য!");
        builder.setMessage("লেখাগুলি কয়েক বার পড়ুন!");
        builder.setNegativeButton("ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}