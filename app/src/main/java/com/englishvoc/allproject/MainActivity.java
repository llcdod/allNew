package com.englishvoc.allproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    CardView page1,page1Game1,page1Game2;

    TextView nameForUser,mainNmae,mPagelesson1,mPagelessonSub1,mPagelessonCoin1,mPagelesson1Game1,mPagelesson1GameSub1,mPagelesson1Game1Coin1,mPagelesson1Game2,mPagelesson1GameSub2,mPagelesson1Game2Coin2;

    String lession0 = "0";
    String lession1 = "1";
    String lession2 = "2";
    String lession3 = "3";
    String lession4 = "4";
    String lession5 = "5";
    String lession6 = "6";
    String lession7 = "7";
    String lession8 = "8";
    String lession9 = "9";
    String lession10 = "10";
    String lession11 = "11";
    String lession12 = "12";

    DatabaseHelper databaseHelper;
    UserDetails userDetails;

    String ID = "1";
    String lession;
    int lessonMain;

    ImageView avaterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //database
        userDetails = new UserDetails();
        databaseHelper = new DatabaseHelper(this);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //=============
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        //headView
        ImageView imageView = (ImageView) headerView.findViewById(R.id.imageView);
        TextView navUsername = (TextView) headerView.findViewById(R.id.mainNmae);
        TextView navUserCoin = (TextView) headerView.findViewById(R.id.userCoin);
        TextView navSub = (TextView) headerView.findViewById(R.id.navSub);
        //main content
        nameForUser = (TextView)findViewById(R.id.nameForUser);
        avaterImageView = (ImageView) findViewById(R.id.avaterViewId);

        mPagelesson1 = (TextView) findViewById(R.id.mPagelesson1);
        mPagelessonSub1 = (TextView) findViewById(R.id.mPagelessonSub1);
        mPagelessonCoin1 = (TextView) findViewById(R.id.mPagelessonCoin1);

        mPagelesson1Game1 = (TextView) findViewById(R.id.mPagelesson1Game1);
        mPagelesson1GameSub1 = (TextView) findViewById(R.id.mPagelesson1GameSub1);
        mPagelesson1Game1Coin1 = (TextView) findViewById(R.id.mPagelesson1Game1Coin1);

        mPagelesson1Game2 = (TextView) findViewById(R.id.mPagelesson1Game2);
        mPagelesson1GameSub2 = (TextView) findViewById(R.id.mPagelesson1GameSub2);
        mPagelesson1Game2Coin2 = (TextView) findViewById(R.id.mPagelesson1Game2Coin2);

        //databse query
        Boolean result = databaseHelper.findUser(ID);

        if(result==true) {

                Cursor cursor = databaseHelper.displayData();
                if (cursor.getCount() == 0) {
                        //no data
                        Toast.makeText(getApplicationContext(), " ( no data ).... ", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                    if(cursor.moveToFirst()){
                        do{
                            String varaible1 = cursor.getString(cursor.getColumnIndex("Name"));
                            String varaible2 = cursor.getString(cursor.getColumnIndex("Profession"));
                            String varaible3 = cursor.getString(cursor.getColumnIndex("Coin"));

                            lession = cursor.getString(cursor.getColumnIndex("Lession"));
                            lessonMain = Integer.parseInt(lession.replaceAll("[\\D]", "")); //convert string to int for upcoming
                            lessionFind();
                            //text
                            navUsername.setText(varaible1);
                            nameForUser.setText(varaible1);
                            navUserCoin.setText(varaible3);
                            navSub.setText(varaible2);
                            //image
                            int image = getResources().getIdentifier(varaible2, "drawable", getPackageName());
                            avaterImageView.setImageResource(image);
                            imageView.setImageResource(image);

                        }while (cursor.moveToNext());
                    }
                    cursor.close();
             }

             Toast.makeText(getApplicationContext(), "id match. lesson "+lession, Toast.LENGTH_LONG).show();
            }else{
               Intent i = new Intent(MainActivity.this, Dhashboard.class);
               startActivity(i);
                Toast.makeText(getApplicationContext(), "id didn't match", Toast.LENGTH_LONG).show();
            }

        //button
        page1 = (CardView) findViewById(R.id.page1);
        page1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                if(lessonMain <= 2 ){
                    Intent i = new Intent(MainActivity.this, Page1.class);
                    startActivity(i);
                } else{
                    sorryGame();
                }
            }
        });
        page1Game1 = (CardView) findViewById(R.id.page1Game1);
        page1Game1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                sorryGame();
            }
        });
        page1Game2 = (CardView) findViewById(R.id.page1Game2);
        page1Game2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                sorryGame();
            }
        });


    }

    public void lessionFind() {
        if(lession.equals(lession0)){
            Intent i = new Intent(MainActivity.this, Dhashboard.class);
            startActivity(i);
        } else {
            mPagelesson1.setText("পাঠ " +lession+ " নিন");           //first button
            mPagelesson1Game1.setText("পাঠ " +lession+ " গেম খেলুন"); //SECOND BUTTON
            mPagelesson1Game2.setText("পাঠ " +lession+ " গেম খেলুন");//Third button
            mPagelessonCoin1.setText(getString(R.string.mPagelessonCoin1));
            if (lession.equals(lession1)) {
                //first button
            //    mPagelesson1.setText(getString(R.string.mPagelesson1));
                mPagelessonSub1.setText(getString(R.string.mPagelessonSub1));
           //     mPagelessonCoin1.setText(getString(R.string.mPagelessonCoin1));
                //SECOND BUTTON
        //        mPagelesson1Game1.setText(getString(R.string.mPagelesson1Game1));
                mPagelesson1GameSub1.setText(getString(R.string.mPagelesson1GameSub1));
         //       mPagelesson1Game1Coin1.setText(getString(R.string.mPagelesson1Game1Coin1));
                //Third button
            //    mPagelesson1Game2.setText(getString(R.string.mPagelesson1Game2));
                mPagelesson1GameSub2.setText(getString(R.string.mPagelesson1GameSub2));
            //    mPagelesson1Game2Coin2.setText(getString(R.string.mPagelesson1Game2Coin2));
            } else if (lession.equals(lession2)) {
                //first button
          //      mPagelesson1.setText(getString(R.string.mPagelesson2));
                mPagelessonSub1.setText(getString(R.string.mPagelessonSub2));
          //      mPagelessonCoin1.setText(getString(R.string.mPagelessonCoin2));
                //SECOND BUTTON
        //        mPagelesson1Game1.setText(getString(R.string.mPagelesson2Game1));
                mPagelesson1GameSub1.setText(getString(R.string.mPagelesson2GameSub1));
         //       mPagelesson1Game1Coin1.setText(getString(R.string.mPagelesson2Game1Coin1));
                //Third button
        //        mPagelesson1Game2.setText(getString(R.string.mPagelesson2Game2));
                mPagelesson1GameSub2.setText(getString(R.string.mPagelesson2GameSub2));
         //       mPagelesson1Game2Coin2.setText(getString(R.string.mPagelesson2Game2Coin2));
            } else {
                mPagelesson1.setText("Up-Coming "+lession);
                mPagelessonSub1.setText("Up-Coming "+lession+" please wait!!!");
                mPagelessonCoin1.setText("Up-Coming "+lession+" please wait!!!");
                //SECOND BUTTON
                mPagelesson1Game1.setText("Up-Coming "+lession);
                mPagelesson1GameSub1.setText("Up-Coming "+lession+" please wait!!!");
                mPagelesson1Game1Coin1.setText("Up-Coming "+lession+" please wait!!!");
                //Third button
                mPagelesson1Game2.setText("Up-Coming "+lession);
                mPagelesson1GameSub2.setText("Up-Coming "+lession+" please wait!!!");
                mPagelesson1Game2Coin2.setText("Up-Coming "+lession+" please wait!!!");
            }
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void sorryGame(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("sorry !");
        builder.setMessage("it will be coming soon !");
        builder.setNegativeButton("ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
