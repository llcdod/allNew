package com.englishvoc.allproject;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {
    //ontime activity
    SharedPreferences sharedPreferences;
    Boolean firstTime;

    //slideadapter
    private ViewPager slideViewPager;
    private LinearLayout linearlayout;

    private TextView[] mdots;
    private SlideAdapter slideAdapter;

    private Button nextBtn,previosBtn;
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //=================
        sharedPreferences = getSharedPreferences("MyPrefs",MODE_PRIVATE);
        firstTime = sharedPreferences.getBoolean("firstTime",true);
        if (firstTime == false){
           Intent i  = new Intent(SplashScreen.this,MainActivity.class);
            startActivity(i);
            finish();
        } else {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            firstTime = false;
            editor.putBoolean("firstTime",firstTime);
            editor.apply();
        }

        //slideadapter next and previous button
        nextBtn = (Button) findViewById(R.id.nextBtn);
        previosBtn = (Button) findViewById(R.id.previosBtn);

        slideViewPager = (ViewPager) findViewById(R.id.slideViewPager);
        linearlayout = (LinearLayout) findViewById(R.id.linearlayout);

        // SliderAdapter = new SliderAdapter(this);
        slideAdapter = new SlideAdapter(this);
        slideViewPager.setAdapter(slideAdapter);
        //slideadapter middle dot next and previous button
        addDotsIndicator(0);
        slideViewPager.addOnPageChangeListener(viewListener);
        //slideadapter next and previous button
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(currentPage == mdots.length - 1){
                    Intent i = new Intent(SplashScreen.this, Dhashboard.class);
                    startActivity(i);
                }else {
                    slideViewPager.setCurrentItem(currentPage + 1);
               }
            }
        });
        previosBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideViewPager.setCurrentItem(currentPage - 1);
            }
        });
    }
    //slideradapter
    public void addDotsIndicator(int position){
        mdots = new TextView[3];
        linearlayout.removeAllViews();

        for(int i = 0; i< mdots.length; i++){
            mdots[i] = new TextView(this);
            mdots[i].setText(Html.fromHtml("&#8226;"));
            mdots[i].setTextSize(35);
            mdots[i].setTextColor(getResources().getColor(R.color.colorWhitetranparent));

            linearlayout.addView(mdots[i]);
        }

        if(mdots.length > 0){
            mdots[position].setTextColor(getResources().getColor(R.color.white));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {

            addDotsIndicator(i);
            currentPage = i;
            if(i == 0){

                nextBtn.setEnabled(true);
                previosBtn.setEnabled(false);
                previosBtn.setVisibility(View.INVISIBLE);

                linearlayout.setVisibility(View.INVISIBLE);

                nextBtn.setText("Next");
                previosBtn.setText("");

            }else if( i == mdots.length - 1 ){

                nextBtn.setEnabled(true);
                previosBtn.setEnabled(true);
                previosBtn.setVisibility(View.VISIBLE);

                linearlayout.setVisibility(View.VISIBLE);

                nextBtn.setText("Finish");
                previosBtn.setText("Back");

            }else{
                nextBtn.setEnabled(true);
                previosBtn.setEnabled(true);
                previosBtn.setVisibility(View.VISIBLE);

                linearlayout.setVisibility(View.VISIBLE);

                nextBtn.setText("Next");
                previosBtn.setText("Back");
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}