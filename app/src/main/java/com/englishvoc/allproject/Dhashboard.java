package com.englishvoc.allproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.englishvoc.allproject.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class Dhashboard extends AppCompatActivity implements View.OnClickListener{
    public Button started;
    private EditText nameEditTextId, ageEditTextId, professionEditTextId;
    //database
    DatabaseHelper databaseHelper;
    UserDetails userDetails;

    //Radio button
    private RadioButton radioStudent, radioHousewife, radioWorking, radionRetired;

    String profession;

    ImageView avaterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dhashboard);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);

        nameEditTextId = (EditText) findViewById(R.id.nameEditTextId);
    //    ageEditTextId = (EditText) findViewById(R.id.ageEditTextId);
     //   professionEditTextId = (EditText) findViewById(R.id.professionEditTextId);

        userDetails = new UserDetails();
        databaseHelper = new DatabaseHelper(this);

        //radioCheck
        radioStudent = (RadioButton) findViewById(R.id.radioStudent);
        radioHousewife = (RadioButton) findViewById(R.id.radioHousewife);
        radioWorking = (RadioButton) findViewById(R.id.radioWorking);
        radionRetired = (RadioButton) findViewById(R.id.radionRetired);

        radioStudent.setOnClickListener(this);
        radioHousewife.setOnClickListener(this);
        radioWorking.setOnClickListener(this);
        radionRetired.setOnClickListener(this);

        avaterImageView = (ImageView) findViewById(R.id.avaterId);

        //Start button
        started = (Button) findViewById(R.id.started);
        Animation animation2 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.downanim);
        started.startAnimation(animation2);
        started.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.radioStudent:
                radioStudent.setChecked(true);
                radioHousewife.setChecked(false);
                radioWorking.setChecked(false);
                radionRetired.setChecked(false);
                profession = "student";
                avaterImageView.setImageResource(R.drawable.aa);
                break;
            case R.id.radioHousewife:

                radioStudent.setChecked(false);
                radioHousewife.setChecked(true);
                radioWorking.setChecked(false);
                radionRetired.setChecked(false);

                profession = "housewife";
                avaterImageView.setImageResource(R.drawable.ic_launcher);
                break;
            case R.id.radioWorking:

                radioStudent.setChecked(false);
                radioHousewife.setChecked(false);
                radioWorking.setChecked(true);
                radionRetired.setChecked(false);
                profession = "business";
                avaterImageView.setImageResource(R.drawable.aa);
                break;
            case R.id.radionRetired:

                radioStudent.setChecked(false);
                radioHousewife.setChecked(false);
                radioWorking.setChecked(false);
                radionRetired.setChecked(true);
                profession = "retired";
                avaterImageView.setImageResource(R.drawable.ic_launcher);

                break;

            case R.id.started:
                //insert database
                String name = nameEditTextId.getText().toString();
                String lession = "1";
                String coin = "0";

                userDetails.setName(name);
                userDetails.setProfession(profession);
                userDetails.setLession(lession);
                userDetails.setCoin(coin);

                long rowId =    databaseHelper.insertData(userDetails);
                if(rowId>0){
                    Intent i = new Intent(Dhashboard.this, MainActivity.class);
                    startActivity(i);
                    Toast.makeText(getApplicationContext(),"Row "+rowId+" is successfully inserted",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(),"Row insertion failed",Toast.LENGTH_LONG).show();
                }
             break;

        }

    }
    //exit
    @Override
    public void onBackPressed()
    {
        finish();
    }
}
