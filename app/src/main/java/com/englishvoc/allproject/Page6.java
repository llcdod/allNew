package com.englishvoc.allproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class Page6 extends AppCompatActivity implements TextToSpeech.OnInitListener,  View.OnClickListener{
    UserDetails userDetails;
    DatabaseHelper databaseHelper;
    AlertDialog.Builder builder;
    private RadioButton lssn1q1, lssn1q2, lssn1q3, lssn1q4;
    LinearLayout lssn1q1laout, lssn1q2laout, lssn1q3laout, lssn1q4laout;
    //database
    String ID = "1";
    String lession;
    String lession1 = "1";
    String lession2 = "2";
    String lession3 = "3";
    String lession4 = "4";
    String lession5 = "5";
    String lession6 = "6";
    String lession7 = "7";
    String lession8 = "8";
    String lession9 = "9";
    String lession10 = "10";
    String lession11 = "11";
    String lession12 = "12";
    TextView l1pge6H1;

    private TextToSpeech tts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page6);
        //FUllscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        databaseHelper = new DatabaseHelper(this);
        //  subHeadingId = (TextView)findViewById(R.id.subHeadingId);
        //radioCheck
        lssn1q1 = (RadioButton) findViewById(R.id.lssn1q1);
        lssn1q2 = (RadioButton) findViewById(R.id.lssn1q2);
        lssn1q3 = (RadioButton) findViewById(R.id.lssn1q3);
        lssn1q4 = (RadioButton) findViewById(R.id.lssn1q4);

        lssn1q1.setOnClickListener(this);
        lssn1q2.setOnClickListener(this);
        lssn1q3.setOnClickListener(this);
        lssn1q4.setOnClickListener(this);

        lssn1q1laout = (LinearLayout) findViewById(R.id.lssn1q1laout);
        lssn1q2laout = (LinearLayout) findViewById(R.id.lssn1q2laout);
        lssn1q3laout = (LinearLayout) findViewById(R.id.lssn1q3laout);
        lssn1q4laout = (LinearLayout) findViewById(R.id.lssn1q4laout);

        //button
        AppCompatButton continuousBotton = (AppCompatButton) findViewById(R.id.continuousBotton);
        continuousBotton.setOnClickListener(this);
        AppCompatButton backBotton = (AppCompatButton) findViewById(R.id.backBotton);
        backBotton.setOnClickListener(this);
        //helpId
        AppCompatButton helpId = (AppCompatButton) findViewById(R.id.helpId);
        helpId.setOnClickListener(this);
        //main containt
        l1pge6H1 = (TextView) findViewById(R.id.l1pge6H1);
        tts = new TextToSpeech(this, this);

        //databse query
        Boolean result = databaseHelper.findUser(ID);
        if (result == true) {
            Cursor cursor = databaseHelper.displayData();
            if (cursor.getCount() == 0) {
                Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
                return;
            } else {
                if (cursor.moveToFirst()) {
                    do {
                        lession = cursor.getString(cursor.getColumnIndex("Lession"));
                        lessionFind();
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            Toast.makeText(getApplicationContext(), "Lession " + lession, Toast.LENGTH_LONG).show();
        } else {
            Intent i = new Intent(Page6.this, Dhashboard.class);
            startActivity(i);
            Toast.makeText(getApplicationContext(), "Lession didn't match", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.lssn1q1:
                /* Button 1*/
                if (lession.equals(lession1)) {
                    speakOut1();
                } else if (lession.equals(lession2)) {
                    speakOut1();
                    //questionButton1false();
                }
                break;
            case R.id.lssn1q2:
                /* Button 2*/
                if (lession.equals(lession1)) {
                    speakOut2();
                } else if (lession.equals(lession2)) {
                    //questionButton2True();
                    speakOut2();
                }
                break;
            case R.id.lssn1q3:
                /* Button 3*/
                if (lession.equals(lession1)) {
                    speakOut3();
                } else if (lession.equals(lession2)) {
                  //  speakOut1();
                    speakOut3();
                } else if (lession.equals(lession3)) {
                 //   questionButton3True();
                }
                break;
            case R.id.lssn1q4:
                /* Button 4*/
                if (lession.equals(lession1)) {
                    speakOut4();
                } else if (lession.equals(lession2)) {
                    speakOut4();
                  //  questionButton4False();
                } else if (lession.equals(lession3)) {
                  //  questionButton4False();
                } else if (lession.equals(lession4)) {
                   // questionButton4True();
                }
                break;
            case R.id.continuousBotton:
                Intent i = new Intent(Page6.this, Page7.class);
                //get data
                Intent intent = getIntent();
                if (intent.hasExtra("coin")) {
                    i.putExtra("coin", "60");
                } else {
                    i.putExtra("coin", "10");
                }
                startActivity(i);
                break;
            case R.id.backBotton:
                Intent i2 = new Intent(Page6.this, Page5.class);
                startActivity(i2);
                break;
            case R.id.helpId:
                helpPage();
                break;
        }
    }
    //TextToSpeech
    private void speakOut1() {
        engLang();
        if (lession.equals(lession1)) {
            tts.speak(getString(R.string.l1page6Q1), TextToSpeech.QUEUE_FLUSH, null);
        } else if (lession.equals(lession2)) {
            tts.speak(getString(R.string.l2page6Q1), TextToSpeech.QUEUE_FLUSH, null);
        }

    }
    private void speakOut2() {
        engLang();
        if (lession.equals(lession1)) {
            tts.speak(getString(R.string.l1page6Q2), TextToSpeech.QUEUE_FLUSH, null);
        } else if (lession.equals(lession2)) {
            tts.speak(getString(R.string.l2page6Q2), TextToSpeech.QUEUE_FLUSH, null);
        }
    }
    private void speakOut3() {
        engLang();
        if (lession.equals(lession1)) {
            tts.speak(getString(R.string.l1page6Q3), TextToSpeech.QUEUE_FLUSH, null);
        } else if (lession.equals(lession2)) {
            tts.speak(getString(R.string.l2page6Q3), TextToSpeech.QUEUE_FLUSH, null);
        }
    }
    private void speakOut4() {
        engLang();
        if (lession.equals(lession1)) {
            tts.speak(getString(R.string.l1page6Q4), TextToSpeech.QUEUE_FLUSH, null);
        } else if (lession.equals(lession2)) {
            tts.speak(getString(R.string.l2page6Q4), TextToSpeech.QUEUE_FLUSH, null);
        }
    }
    @Override
    public void onInit(int status) {

        // TODO Auto-generated method stub
        // TTS is successfully initialized
        if (status == TextToSpeech.SUCCESS) {
            //English
            engLang();
        } else {
            Toast.makeText(this, "TTS Initilization Failed", Toast.LENGTH_LONG)
                    .show();
        }
    }
    public void engLang(){
        // Setting speech language
        int result = tts.setLanguage(Locale.US);
        // If your device doesn't support language you set above
        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            // Cook simple toast message with message
            Toast.makeText(getApplicationContext(), "Language not supported",
                    Toast.LENGTH_LONG).show();
        }
        // Enable the button - It was disabled in main.xml (Go back and
        // Check it)
        else {
           lssn1q1.setEnabled(true);
            lssn1q2.setEnabled(true);
            lssn1q3.setEnabled(true);
            lssn1q4.setEnabled(true);
        }
        // TTS is not initialized properly
    }
    /*--------------------------------------------------------------------------------------*/
    //find lession
    public void lessionFind() {
        if (lession.equals(lession1)) {
            l1pge6H1.setText(getString(R.string.l1pge6H1));
            lssn1q1.setText(getString(R.string.l1page6Q1));
            lssn1q2.setText(getString(R.string.l1page6Q2));
            lssn1q3.setText(getString(R.string.l1page6Q3));
            lssn1q4.setText(getString(R.string.l1page6Q4));
        } else if (lession.equals(lession2)) {
            l1pge6H1.setText(getString(R.string.l2pge6H1));
            lssn1q1.setText(getString(R.string.l2page6Q1));
            lssn1q2.setText(getString(R.string.l2page6Q2));
            lssn1q3.setText(getString(R.string.l2page6Q3));
            lssn1q4.setText(getString(R.string.l2page6Q4));
        } else if (lession.equals(lession3)) {

        }
    }
    public void helpPage(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(Page6.this);
        builder.setTitle("Help !");
        builder.setMessage("প্রতিটা বোতামে স্পরশ করে মনযোগ দিয়ে শুনুন!");
        builder.setNegativeButton("ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}